package hk.quantr.peterswing.advancedswing.ribbonbar;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class JRibbonBigButton extends JButton {

	public JRibbonBigButton() {
		initGUI();
	}

	public JRibbonBigButton(Icon icon) {
		super(icon);
		initGUI();
	}

	public JRibbonBigButton(String name) {
		super(name);
		initGUI();
	}

	public JRibbonBigButton(Action a) {
		super(a);
		initGUI();
	}

	public JRibbonBigButton(String text, Icon icon) {
		super(text, icon);
		initGUI();
	}

	private void initGUI() {
		setVerticalTextPosition(SwingConstants.BOTTOM);
		setHorizontalTextPosition(SwingConstants.CENTER);
		putClientProperty("buttonType", "ribbonButton");
	}

}

package hk.quantr.peterswing.advancedswing.pager;

import java.util.EventObject;

public class PagerTextFieldEvent extends EventObject {
	private int keyCode;
	private String value;

	public PagerTextFieldEvent(Object source) {
		super(source);
	}

	public int getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

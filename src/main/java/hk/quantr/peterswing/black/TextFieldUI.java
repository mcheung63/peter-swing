/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *        Liquid Look and Feel                                                   *
 *                                                                              *
 *  Author, Miroslav Lazarevic                                                  *
 *                                                                              *
 *   For licensing information and credits, please refer to the                 *
 *   comment in file com.birosoft.liquid.LiquidLookAndFeel                      *
 *                                                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
package hk.quantr.peterswing.black;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.text.JTextComponent;

import hk.quantr.peterswing.toedter.calendar.JYearChooser;

public class TextFieldUI extends BasicTextFieldUI implements FocusListener {
	// static JTextComponent _editor;
	// boolean onFocus;

	// ///////////////////////////////////////////////////////////////////////
	Image onFocus_upperLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_upperLeft.png")).getImage();
	Image onFocus_middleLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_middleLeft.png")).getImage();
	Image onFocus_lowerLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_lowerLeft.png")).getImage();
	Image onFocus_middleUpper = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_middleUpper.png")).getImage();
	Image onFocus_middleLower = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_middleLower.png")).getImage();
	Image onFocus_upperRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_upperRight.png")).getImage();
	Image onFocus_middleRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_middleRight.png")).getImage();
	Image onFocus_lowerRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/onFocus/PTextField_onFocus_lowerRight.png")).getImage();

	// ///////////////////////////////////////////////////////////////////////
	Image lostFocus_upperLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperLeft.png")).getImage();
	Image lostFocus_middleLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLeft.png")).getImage();
	Image lostFocus_lowerLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerLeft.png")).getImage();
	Image lostFocus_middleUpper = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleUpper.png")).getImage();
	Image lostFocus_middleLower = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLower.png")).getImage();
	Image lostFocus_upperRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperRight.png")).getImage();
	Image lostFocus_middleRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleRight.png")).getImage();
	Image lostFocus_lowerRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerRight.png")).getImage();

	// ///////////////////////////////////////////////////////////////////////
	Image lostFocus_upperLeft_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperLeft_red.png")).getImage();
	Image lostFocus_middleLeft_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLeft_red.png")).getImage();
	Image lostFocus_lowerLeft_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerLeft_red.png")).getImage();
	Image lostFocus_middleUpper_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleUpper_red.png")).getImage();
	Image lostFocus_middleLower_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLower_red.png")).getImage();
	Image lostFocus_upperRight_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperRight_red.png")).getImage();
	Image lostFocus_middleRight_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleRight_red.png")).getImage();
	Image lostFocus_lowerRight_red = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerRight_red.png")).getImage();

	// ///////////////////////////////////////////////////////////////////////
	Image lostFocus_upperLeft_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperLeft_brighter.png")).getImage();
	Image lostFocus_middleLeft_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLeft_brighter.png")).getImage();
	Image lostFocus_lowerLeft_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerLeft_brighter.png")).getImage();
	Image lostFocus_middleUpper_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleUpper_brighter.png")).getImage();
	Image lostFocus_middleLower_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLower_brighter.png")).getImage();
	Image lostFocus_upperRight_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperRight_brighter.png")).getImage();
	Image lostFocus_middleRight_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleRight_brighter.png")).getImage();
	Image lostFocus_lowerRight_brighter = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerRight_brighter.png")).getImage();

	public static ComponentUI createUI(JComponent c) {
		return new TextFieldUI();
	}

	public void installUI(JComponent c) {
		super.installUI(c);
		// c.addMouseListener(this);
		c.setBorder(new EmptyBorder(2, 4, 2, 4));
	}

	protected void installDefaults() {
		super.installDefaults();

		getComponent().addFocusListener(this);
	}

	protected void uninstallDefaults() {
		getComponent().removeFocusListener(this);

		super.uninstallDefaults();
	}

	protected void paintBackground(Graphics g) {
		JTextComponent editor = getComponent();

		if (editor.isEnabled()) {
			editor.setBackground(Color.white);
			g.setColor(Color.white);
			g.fillRect(0, 0, editor.getWidth(), editor.getHeight());
		} else {
			editor.setBackground(new Color(230, 230, 230));
			g.setColor(new Color(230, 230, 230));
			g.fillRect(0, 0, editor.getWidth(), editor.getHeight());
		}

		boolean noBorder = false;
		if (editor.getClientProperty("NoBorder") != null) {
			noBorder = (boolean) editor.getClientProperty("NoBorder");
		}
		boolean brighter = false;
		if (editor.getClientProperty("brighter") != null) {
			brighter = (boolean) editor.getClientProperty("brighter");
		}
		boolean hightlightRed = false;
		if (editor.getClientProperty("hightlightRed") != null) {
			hightlightRed = (boolean) editor.getClientProperty("hightlightRed");
		}

		String placeHolder = null;
		if (editor.getClientProperty("placeHolder") != null) {
			placeHolder = (String) editor.getClientProperty("placeHolder");
		}

		if (!(editor.getParent() instanceof DefaultEditor) && !(editor.getParent().getParent() instanceof JYearChooser)) {
			if (this.getComponent().hasFocus() && editor.isEditable()) {
				g.drawImage(onFocus_upperLeft, 0, 0, 2, 2, null);
				g.drawImage(onFocus_middleLeft, 0, 2, 2, editor.getHeight() - 4, null);
				g.drawImage(onFocus_lowerLeft, 0, editor.getHeight() - 2, 2, 2, null);

				g.drawImage(onFocus_middleUpper, 2, 0, editor.getWidth() - 4, 2, null);
				g.drawImage(onFocus_middleLower, 2, editor.getHeight() - 2, editor.getWidth() - 4, 2, null);

				g.drawImage(onFocus_upperRight, editor.getWidth() - 2, 0, 2, 2, null);
				g.drawImage(onFocus_middleRight, editor.getWidth() - 2, 2, 2, editor.getHeight() - 4, null);
				g.drawImage(onFocus_lowerRight, editor.getWidth() - 2, editor.getHeight() - 2, 2, 2, null);
			} else if (brighter && editor.isEditable() && editor.isEnabled()) {
				//				g.drawImage(CommonLib.brighter(lostFocus_upperLeft), 0, 0, 2, 2, null);
				//				g.drawImage(CommonLib.brighter(lostFocus_middleLeft), 0, 2, 2, editor.getHeight() - 4, null);
				//				g.drawImage(CommonLib.brighter(lostFocus_lowerLeft), 0, editor.getHeight() - 2, 2, 2, null);
				//
				//				g.drawImage(CommonLib.brighter(lostFocus_middleUpper), 2, 0, editor.getWidth() - 4, 2, null);
				//				g.drawImage(CommonLib.brighter(lostFocus_middleLower), 2, editor.getHeight() - 2, editor.getWidth() - 4, 2, null);
				//
				//				g.drawImage(CommonLib.brighter(lostFocus_upperRight), editor.getWidth() - 2, 0, 2, 2, null);
				//				g.drawImage(CommonLib.brighter(lostFocus_middleRight), editor.getWidth() - 2, 2, 2, editor.getHeight() - 4, null);
				//				g.drawImage(CommonLib.brighter(lostFocus_lowerRight), editor.getWidth() - 2, editor.getHeight() - 2, 2, 2, null);

				g.drawImage(lostFocus_upperLeft_brighter, 0, 0, 2, 2, null);
				g.drawImage(lostFocus_middleLeft_brighter, 0, 2, 2, editor.getHeight() - 4, null);
				g.drawImage(lostFocus_lowerLeft_brighter, 0, editor.getHeight() - 2, 2, 2, null);

				g.drawImage(lostFocus_middleUpper_brighter, 2, 0, editor.getWidth() - 4, 2, null);
				g.drawImage(lostFocus_middleLower_brighter, 2, editor.getHeight() - 2, editor.getWidth() - 4, 2, null);

				g.drawImage(lostFocus_upperRight_brighter, editor.getWidth() - 2, 0, 2, 2, null);
				g.drawImage(lostFocus_middleRight_brighter, editor.getWidth() - 2, 2, 2, editor.getHeight() - 4, null);
				g.drawImage(lostFocus_lowerRight_brighter, editor.getWidth() - 2, editor.getHeight() - 2, 2, 2, null);
			} else if (!noBorder && editor.isEditable() && editor.isEnabled()) {
				if (hightlightRed) {
					g.drawImage(lostFocus_upperLeft_red, 0, 0, 2, 2, null);
					g.drawImage(lostFocus_middleLeft_red, 0, 2, 2, editor.getHeight() - 4, null);
					g.drawImage(lostFocus_lowerLeft_red, 0, editor.getHeight() - 2, 2, 2, null);

					g.drawImage(lostFocus_middleUpper_red, 2, 0, editor.getWidth() - 4, 2, null);
					g.drawImage(lostFocus_middleLower_red, 2, editor.getHeight() - 2, editor.getWidth() - 4, 2, null);

					g.drawImage(lostFocus_upperRight_red, editor.getWidth() - 2, 0, 2, 2, null);
					g.drawImage(lostFocus_middleRight_red, editor.getWidth() - 2, 2, 2, editor.getHeight() - 4, null);
					g.drawImage(lostFocus_lowerRight_red, editor.getWidth() - 2, editor.getHeight() - 2, 2, 2, null);
				} else {
					g.drawImage(lostFocus_upperLeft, 0, 0, 2, 2, null);
					g.drawImage(lostFocus_middleLeft, 0, 2, 2, editor.getHeight() - 4, null);
					g.drawImage(lostFocus_lowerLeft, 0, editor.getHeight() - 2, 2, 2, null);

					g.drawImage(lostFocus_middleUpper, 2, 0, editor.getWidth() - 4, 2, null);
					g.drawImage(lostFocus_middleLower, 2, editor.getHeight() - 2, editor.getWidth() - 4, 2, null);

					g.drawImage(lostFocus_upperRight, editor.getWidth() - 2, 0, 2, 2, null);
					g.drawImage(lostFocus_middleRight, editor.getWidth() - 2, 2, 2, editor.getHeight() - 4, null);
					g.drawImage(lostFocus_lowerRight, editor.getWidth() - 2, editor.getHeight() - 2, 2, 2, null);
				}
			} else if (!editor.isEnabled()) {
				g.drawImage(lostFocus_upperLeft, 0, 0, 2, 2, null);
				g.drawImage(lostFocus_middleLeft, 0, 2, 2, editor.getHeight() - 4, null);
				g.drawImage(lostFocus_lowerLeft, 0, editor.getHeight() - 2, 2, 2, null);

				g.drawImage(lostFocus_middleUpper, 2, 0, editor.getWidth() - 4, 2, null);
				g.drawImage(lostFocus_middleLower, 2, editor.getHeight() - 2, editor.getWidth() - 4, 2, null);

				g.drawImage(lostFocus_upperRight, editor.getWidth() - 2, 0, 2, 2, null);
				g.drawImage(lostFocus_middleRight, editor.getWidth() - 2, 2, 2, editor.getHeight() - 4, null);
				g.drawImage(lostFocus_lowerRight, editor.getWidth() - 2, editor.getHeight() - 2, 2, 2, null);
			}
			if (placeHolder != null && (editor.getText() == null || editor.getText().equals(""))) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

				g.setColor(new Color(100, 100, 100));
				int textfieldHeight = editor.getHeight() - 4;
				int realFontHeight = editor.getFont().getSize();
				g.drawString(placeHolder, 6, textfieldHeight - ((textfieldHeight - realFontHeight) / 2));
			}
		}
	}

	public void focusGained(FocusEvent e) {
		getComponent().repaint();
	}

	public void focusLost(FocusEvent e) {
		getComponent().repaint();
	}
}

package hk.quantr.peterswing.black;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPanelUI;

import hk.quantr.peterswing.CommonLib;
import hk.quantr.peterswing.advancedswing.carousel.Carousel;

public class PanelUI extends BasicPanelUI {
	private static PanelUI panelUI;
	static Image bg = new ImageIcon(Carousel.class.getResource("/hk/quantr/peterswing/black/mainBG.png")).getImage();

	public static ComponentUI createUI(JComponent c) {
		if (panelUI == null) {
			panelUI = new PanelUI();
		}
		return panelUI;
	}

	public void installUI(JComponent c) {
		super.installUI(c);
	}

	public void uninstallUI(JComponent c) {
		super.uninstallUI(c);
	}

	@Override
	public void paint(Graphics g, JComponent c) {
		CommonLib.paintBackgorund(g, bg, c.getWidth(), c.getHeight());
		super.paint(g, c);
	}

	//	public void paint(Graphics g, JComponent c) {
	//		super.paint(g, c);

	//		g.setColor(Color.white);
	//		g.fillRect(0, 0, c.getWidth(), c.getHeight());

	//		boolean disable = CommonLib.isDisable(c);
	//		if (lastDisableStatus == null || lastDisableStatus != disable) {
	//			CommonLib.enableJComponent(c, disable);
	//			lastDisableStatus = disable;
	//		}

	//		if (!disable) {
	//			for (Component com : c.getComponents()) {
	//				com.setVisible(true);
	//			}
	//			super.paint(g, c);
	//		} else {
	//			for (Component com : c.getComponents()) {
	//				com.setVisible(false);
	//			}
	//			//			c.setOpaque(true);
	//			//			c.setBackground(Color.black);
	//			g.setColor(new Color(0, 0, 0, 0.5f));
	//			g.fillRect(0, 0, c.getWidth(), c.getHeight());
	//		}
	//	}
}

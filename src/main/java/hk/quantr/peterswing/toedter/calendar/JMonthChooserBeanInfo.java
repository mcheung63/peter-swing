package hk.quantr.peterswing.toedter.calendar;

import hk.quantr.peterswing.toedter.components.GenericBeanInfo;

public class JMonthChooserBeanInfo extends GenericBeanInfo {
	public JMonthChooserBeanInfo() {
		super("JMonthChooser", true);
	}
}
package hk.quantr.peterswing.toedter.calendar;

import hk.quantr.peterswing.toedter.components.GenericBeanInfo;

public class JDateChooserBeanInfo extends GenericBeanInfo {
	public JDateChooserBeanInfo() {
		super("JDateChooser", true);
	}
}
package hk.quantr.peterswing.toedter.calendar;

import hk.quantr.peterswing.toedter.components.GenericBeanInfo;

public class JCalendarBeanInfo extends GenericBeanInfo {
	public JCalendarBeanInfo() {
		super("JCalendar", true);
	}
}
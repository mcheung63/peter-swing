package hk.quantr.peterswing.toedter.calendar;

import hk.quantr.peterswing.toedter.components.GenericBeanInfo;

public class JDayChooserBeanInfo extends GenericBeanInfo {
	public JDayChooserBeanInfo() {
		super("JDayChooser", true);
	}
}
package hk.quantr.peterswing.toedter.calendar;

import hk.quantr.peterswing.toedter.components.GenericBeanInfo;

public class JYearChooserBeanInfo extends GenericBeanInfo {
	public JYearChooserBeanInfo() {
		super("JYearChooser", false);
	}
}
package hk.quantr.peterswing.toedter.calendar.demo;

import hk.quantr.peterswing.toedter.components.GenericBeanInfo;

public class DateChooserPanelBeanInfo extends GenericBeanInfo {

	public DateChooserPanelBeanInfo() {
		super("JDateChooser", true);
	}
}
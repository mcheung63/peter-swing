package hk.quantr.peterswing.toedter.components;

public class JSpinFieldBeanInfo extends GenericBeanInfo {
	public JSpinFieldBeanInfo() {
		super("JSpinField", false);
	}
}
package hk.quantr.peterswing.toedter.components;

public class JLocaleChooserBeanInfo extends GenericBeanInfo {
	public JLocaleChooserBeanInfo() {
		super("JLocaleChooser", true);
	}
}
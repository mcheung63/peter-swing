package hk.quantr.peterswing.captcha;

public enum EncryptMethod {
	MD5, SHA1, SHA256, SHA384, SHA512
}

package hk.quantr.peterswing.captcha;

public class OptionErrorException extends Exception{

	private static final long serialVersionUID = -264652021909754717L;

	public OptionErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public OptionErrorException(String message) {
		super(message);
	}

	public OptionErrorException(Throwable cause) {
		super(cause);
	}
}

package hk.quantr.peterswing.captcha;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class Encryptor {

	public static String encryptString(String s, EncryptMethod encryptMethod) {
		switch (encryptMethod) {
		case MD5	: s = giveMD5String(s); break;
		case SHA1	: s = giveSHA1String(s); break;
		case SHA256	: s = giveSHA256String(s); break;
		case SHA384	: s = giveSHA384String(s); break;
		case SHA512	: s = giveSHA512String(s); break;
		}
		return s;
	}

	public static String encryptString(String s, List<EncryptMethod> encryptMethods) {
		for (EncryptMethod encryptMethod : encryptMethods) {
			s = encryptString(s, encryptMethod);
		}
		return s;
	}

	private static String giveMD5String(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private static String giveSHA1String(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private static String giveSHA256String(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private static String giveSHA384String(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-384");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private static String giveSHA512String(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] messageDigest = md.digest(s.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
}

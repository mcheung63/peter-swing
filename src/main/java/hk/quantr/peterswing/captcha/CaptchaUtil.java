package hk.quantr.peterswing.captcha;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

public class CaptchaUtil {

	public static String encryptString(String s) {
		List<EncryptMethod> encryptMethods = new ArrayList<EncryptMethod>();
		encryptMethods.add(EncryptMethod.MD5);
		encryptMethods.add(EncryptMethod.MD5);
		encryptMethods.add(EncryptMethod.MD5);
		encryptMethods.add(EncryptMethod.SHA256);
		encryptMethods.add(EncryptMethod.MD5);
		s = Encryptor.encryptString(s, encryptMethods);
		return s;
	}

	public static boolean dencryptString(String encryptedString, String compareString, boolean caseSensitive) {
		if (encryptedString == null || "".equals(encryptedString) || compareString == null || "".equals(compareString)) return false;
		if (!caseSensitive) compareString = compareString.toUpperCase();
		List<EncryptMethod> encryptMethods = new ArrayList<EncryptMethod>();
		encryptMethods.add(EncryptMethod.MD5);
		encryptMethods.add(EncryptMethod.MD5);
		encryptMethods.add(EncryptMethod.MD5);
		encryptMethods.add(EncryptMethod.SHA256);
		encryptMethods.add(EncryptMethod.MD5);
		compareString = Encryptor.encryptString(compareString, encryptMethods);
		return compareString.equals(encryptedString);
	}

	public static void exportCaptcha(BufferedImage image, String path, String fileName) throws Exception {
		exportImageToFile(image, fileName, "png", path);
	}

	public static void exportCaptcha(List<BufferedImage> images, String path, String fileName) throws Exception {
		for (BufferedImage image : images) {
			exportImageToFile(image, fileName, "png", path);
		}
	}

	private static void exportImageToFile(BufferedImage image, String name, String format, String path) throws Exception {
		try {
			if (!path.endsWith("/")) path += "/";
			File outputfile = new File(path + name + "." + format);
			if (!outputfile.exists()) outputfile.mkdirs();
			ImageIO.write(image, format, outputfile);
		} catch (Exception e) {
			throw new Exception();
		}
	}
}

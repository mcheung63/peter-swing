package hk.quantr.peterswing.captcha;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import hk.quantr.peterswing.CommonLib;

public class CaptchaGenerator {
	final static int paddingTopBottom = 10;
	final static int paddingLeftRight = 3;
	final static int noise = 20;
	final static int captchaHeight = 50;
	final static String fonts[] = { "Times", "Times New Roman", "Georgia", "serif", "Verdana", "Arial", "Helvetica", "sans-serif", "Lucida Console", "Courier", "monospace",
			"cursive", "fantasy" };
	//	final static List<Color> colors = Arrays.asList(Color.RED, Color.YELLOW, Color.BLUE, Color.CYAN, Color.GREEN, Color.MAGENTA, Color.PINK, Color.ORANGE);
	public static List<Color> colors = Arrays.asList(Color.RED, Color.BLUE);

	public static Captcha noisedCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive) throws OptionErrorException {
		String s = CommonLib.randomString(count, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		Color color = null;
		Color defaultColor = colors.get(new Random().nextInt(colors.size()));

		for (int i = 0; i < charArray.length; i++) {
			if (i < 3) {
				do {
					color = colors.get(new Random().nextInt(colors.size()));
					;
				} while (color.equals(defaultColor));
			} else {
				color = defaultColor;
			}

			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);
		finalImage = noise(finalImage, noise);

		captcha.setImage(finalImage);
		return captcha;
	}

	public static Captcha coloredCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive) throws OptionErrorException {
		String s = CommonLib.randomString(count + 3, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s.substring(3)));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		Color color = null;
		Color defaultColor = colors.get(new Random().nextInt(colors.size()));

		for (int i = 0; i < charArray.length; i++) {
			if (i < 3) {
				do {
					color = colors.get(new Random().nextInt(colors.size()));
					;
				} while (color.equals(defaultColor));
			} else {
				color = defaultColor;
			}

			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);

		captcha.setImage(finalImage);
		return captcha;
	}

	public static Captcha linedCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive, int width, int height)
			throws OptionErrorException {
		String s = CommonLib.randomString(count, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		captcha.code = s;
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		for (int i = 0; i < charArray.length; i++) {
			Color color = colors.get(new Random().nextInt(colors.size()));
			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);
		finalImage = drawRandomLines(finalImage);
		try {
			finalImage = CommonLib.resizeImage(finalImage, width, height, BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			e.printStackTrace();
		}
		captcha.setImage(finalImage);
		return captcha;
	}

	public static Captcha noisedLinedCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive, int width, int height)
			throws OptionErrorException {
		String s = CommonLib.randomString(count, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		//		captcha.setCode(s);
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		for (int i = 0; i < charArray.length; i++) {
			Color color = colors.get(new Random().nextInt(colors.size()));
			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);
		finalImage = drawRandomLines(finalImage);
		finalImage = noise(finalImage, noise);
		try {
			finalImage = CommonLib.resizeImage(finalImage, width, height, BufferedImage.TYPE_INT_RGB);
		} catch (IOException e) {
			e.printStackTrace();
		}
		captcha.setImage(finalImage);
		return captcha;
	}

	public static Captcha noisedColoredCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive) throws OptionErrorException {
		String s = CommonLib.randomString(count + 3, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s.substring(3)));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		Color color = null;
		Color defaultColor = colors.get(new Random().nextInt(colors.size()));

		for (int i = 0; i < charArray.length; i++) {
			if (i < 3) {
				do {
					color = colors.get(new Random().nextInt(colors.size()));
					;
				} while (color.equals(defaultColor));
			} else {
				color = defaultColor;
			}

			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);
		finalImage = noise(finalImage, noise);

		captcha.setImage(finalImage);
		return captcha;
	}

	public static Captcha coloredLinedCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive) throws OptionErrorException {
		String s = CommonLib.randomString(count + 3, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s.substring(3)));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		Color color = null;
		Color defaultColor = colors.get(new Random().nextInt(colors.size()));

		for (int i = 0; i < charArray.length; i++) {
			if (i < 3) {
				do {
					color = colors.get(new Random().nextInt(colors.size()));
					;
				} while (color.equals(defaultColor));
			} else {
				color = defaultColor;
			}

			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);
		finalImage = drawRandomLines(finalImage);

		captcha.setImage(finalImage);
		return captcha;
	}

	public static Captcha allInOneCaptcha(Color backgroundColor, int count, boolean numberic, boolean alphabetic, boolean caseSensitive) throws OptionErrorException {
		String s = CommonLib.randomString(count + 3, numberic, alphabetic, caseSensitive);
		Captcha captcha = new Captcha();
		captcha.setEncryptedCode(CaptchaUtil.encryptString(s.substring(3)));
		char[] charArray = s.toCharArray();

		List<BufferedImage> images = new ArrayList<BufferedImage>();
		int totalWidth = 0;

		BufferedImage finalImage;
		Color color = null;
		Color defaultColor = colors.get(new Random().nextInt(colors.size()));

		for (int i = 0; i < charArray.length; i++) {
			if (i < 3) {
				do {
					color = colors.get(new Random().nextInt(colors.size()));
					;
				} while (color.equals(defaultColor));
			} else {
				color = defaultColor;
			}

			BufferedImage image = CommonLib.stringToBufferedImage(String.valueOf(charArray[i]), fonts[new Random().nextInt(fonts.length)], (new Random().nextInt(15) + 20), color);
			image = rotateImage(image, (new Random().nextInt(90) - 45));
			totalWidth += image.getWidth();
			images.add(image);
		}
		finalImage = combineImages(backgroundColor, images, totalWidth);
		finalImage = drawRandomLines(finalImage);
		finalImage = noise(finalImage, noise);

		captcha.setImage(finalImage);
		return captcha;
	}

	private static BufferedImage noise(BufferedImage image, double scale) {
		// Nested loop over every pixel
		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				// Get current color; add noise to each channel
				Color color = new Color(image.getRGB(x, y));
				int red = (int) (constrain(color.getRed() + scale * (2 * Math.random() - 1), 0, 255));
				int green = (int) (constrain(color.getGreen() + scale * (2 * Math.random() - 1), 0, 255));
				int blue = (int) (constrain(color.getBlue() + scale * (2 * Math.random() - 1), 0, 255));
				// Put new color
				Color newColor = new Color(red, green, blue);
				image.setRGB(x, y, newColor.getRGB());
			}
		}
		return image;
	}

	private static BufferedImage rotateImage(BufferedImage image, int tangent) {
		BufferedImage rotatedImage = new BufferedImage((int) Math.ceil(image.getWidth()) + paddingLeftRight * 2, (int) Math.ceil(image.getHeight()), BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = rotatedImage.getGraphics();
		Graphics2D g2d = (Graphics2D) g;

		double rotationRequired = Math.toRadians(tangent);
		double locationX = image.getWidth() / 2;
		double locationY = image.getHeight() / 2;
		AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

		g2d.drawImage(op.filter(image, null), 0, 0, null);
		g2d.dispose();
		g.dispose();
		return rotatedImage;
	}

	private static BufferedImage drawRandomLines(BufferedImage image) {
		Graphics g = image.getGraphics();
		Graphics2D g2d = (Graphics2D) g;
		CubicCurve2D c = new CubicCurve2D.Double();

		int randNum = new Random().nextInt(2) + 2;
		for (int i = 0; i < randNum; i++) {
			c.setCurve(0, (image.getHeight() / randNum * i), new Random().nextInt(image.getWidth()), new Random().nextInt(image.getHeight()),
					new Random().nextInt(image.getWidth()), new Random().nextInt(image.getHeight()), image.getWidth(), (image.getHeight() / randNum * (randNum - i)));
			g2d.setPaint(colors.get(new Random().nextInt(colors.size())));
			g2d.draw(c);
		}

		g.dispose();
		g2d.dispose();

		return image;
	}

	private static BufferedImage combineImages(Color backgroundColor, List<BufferedImage> images, int imageWidth) {
		BufferedImage finalImage = new BufferedImage(imageWidth, captchaHeight, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = finalImage.getGraphics();
		g.setColor(backgroundColor);
		g.fillRect(0, 0, finalImage.getWidth(), captchaHeight);

		int x = 0;
		for (BufferedImage image : images) {
			g.drawImage(image, x, 0, null);
			x += image.getWidth();
		}
		g.dispose();

		return finalImage;
	}

	private static double constrain(double val, double min, double max) {
		if (val < min) {
			return min;
		} else if (val > max) {
			return max;
		}
		return val;
	}
}

package hk.quantr.peterswing.captcha;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Captcha {

	private BufferedImage image;
	private String encryptedCode;
	public String code;

	public BufferedImage getImage() {
		return image;
	}

	public ImageIcon getImageIcon() {
		return new ImageIcon(image);
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public String getEncryptedCode() {
		return encryptedCode;
	}

	public void setEncryptedCode(String encryptedCode) {
		this.encryptedCode = encryptedCode;
	}
}

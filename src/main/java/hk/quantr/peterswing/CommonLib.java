package hk.quantr.peterswing;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.lang3.RandomStringUtils;

import hk.quantr.peterswing.captcha.OptionErrorException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.StringReader;
import javax.swing.Icon;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.codehaus.jackson.map.ObjectMapper;

public class CommonLib {

	public static void main(String args[]) {
		int count = getNumberOfFile(new File("/Users/peter/NetBeansProjects/PeterI"), new String[]{".c", ".cc", ".cpp"});
		System.out.println("count=" + count);
	}

	public static boolean isNumeric(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (!(s.charAt(i) >= '0' && s.charAt(i) <= '9')) {
				return false;
			}
		}
		return true;
	}

	public static byte[] readFileByte(File file, long offset, int size) {
		return readFileByte(file.getAbsolutePath(), offset, size);
	}

	public static byte[] readFileByte(String filepath, long offset, int size) {
		try {
			RandomAccessFile br = new RandomAccessFile(filepath, "r");
			br.seek(offset);
			byte bytes[] = new byte[size];
			br.readFully(bytes);
			br.close();
			return bytes;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static BigInteger string2BigInteger(String s) {
		if (s == null) {
			return BigInteger.valueOf(0);
		} else {
			s = s.trim().toLowerCase();
			if (s.startsWith("0x")) {
				return new BigInteger(s.substring(2), 16);
			} else if (s.matches(".*[abcdefABCDEF].*")) {
				return new BigInteger(s, 16);
			} else {
				return new BigInteger(s, 10);
			}
		}
	}

	public static byte[] string2bytes(String s) {
		if (s == null) {
			return null;
		} else {
			s = s.trim().toLowerCase();

			if (s.startsWith("0x")) {
				if (s.length() <= 4) {
					return new byte[]{(byte) string2int(s)};
				} else if (s.length() <= 6) {
					return string2word(s);
				} else if (s.length() <= 10) {
					long a = string2long(s);
					return new byte[]{(byte) (a & 0xff), (byte) ((a >> 8) & 0xff), (byte) ((a >> 16) & 0xff), (byte) ((a >> 24) & 0xff)};
				} else {
					return null;
				}
			} else {
				Long l = Long.parseLong(s);
				s = Long.toHexString(l);
				if (s.length() <= 2) {
					return new byte[]{(byte) string2int(s)};
				} else if (s.length() <= 4) {
					return string2word(s);
				} else {
					return null;
				}
			}
		}
	}

	public static int string2int(String s) {
		boolean negative = false;
		if (s.charAt(0) == '-') {
			negative = true;
			s = s.substring(1);
		}
		if (s == null) {
			return 0;
		} else {
			s = s.trim().toLowerCase();
			if (s.startsWith("0x")) {
				return (int) Long.parseLong(s.substring(2), 16) * (negative ? -1 : 1);
			} else if (s.matches(".*[abcdefABCDEF].*")) {
				return (int) Long.parseLong(s, 16) * (negative ? -1 : 1);
			} else {
				return (int) Long.parseLong(s, 10) * (negative ? -1 : 1);
			}
		}
	}

	public static long string2long(String s) {
		boolean negative = false;
		if (s.charAt(0) == '-') {
			negative = true;
			s = s.substring(1);
		}
		if (s == null) {
			return 0L;
		} else {
			s = s.trim().toLowerCase();
			if (s.startsWith("0x")) {
				return Long.parseLong(s.substring(2), 16) * (negative ? -1 : 1);
			} else if (s.matches(".*[abcdefABCDEF].*")) {
				return Long.parseLong(s, 16) * (negative ? -1 : 1);
			} else {
				return Long.parseLong(s, 10) * (negative ? -1 : 1);
			}
		}
	}

	public static byte[] string2word(String s) {
		if (s == null) {
			return null;
		} else {
			s = s.trim().toLowerCase();
			long x;
			if (s.startsWith("0x")) {
				x = Long.parseLong(s.substring(2), 16);
			} else if (s.matches(".*[abcdefABCDEF].*")) {
				x = Long.parseLong(s, 16);
			} else {
				x = Long.parseLong(s, 10);
			}
			return new byte[]{(byte) (x & 0xff), (byte) ((x >> 8) & 0xff)};
		}
	}

//	public static byte string2byte(String s) {
//		if (s == null) {
//			return 0;
//		} else {
//			s = s.trim().toLowerCase();
//			if (s.startsWith("0x")) {
//				return (byte)Integer.parseInt(s.substring(2), 16) & 0xff;
//			} else if (s.matches(".*[abcdefABCDEF].*")) {
//				return Integer.parseInt(s, 16) & 0xff;
//			} else {
//				return Integer.parseInt(s, 10) & 0xff;
//			}
//		}
//	}
	public static boolean isNumber(String s) {
		try {
			BigInteger l = string2BigInteger(s);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static boolean isFloat(String s) {
		try {
			Float.parseFloat(s);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static long convertFilesize(String filesize) {
		filesize = filesize.trim().toLowerCase();

		if (filesize == null) {
			return 0;
		}
		int isHex = 10;
		if (filesize.startsWith("0x")) {
			filesize = filesize.substring(2);
			isHex = 16;
		}
		long returnValue = -1;
		if (filesize.length() == 0) {
			return 0;
		} else if (filesize.length() == 1) {
			return Long.parseLong(filesize, isHex);
		} else if (filesize.substring(filesize.length() - 1).toLowerCase().equals("t")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 1).trim(), isHex) * 1024 * 1024 * 1024 * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 2).toLowerCase().equals("tb")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 2).trim(), isHex) * 1024 * 1024 * 1024 * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 1).toLowerCase().equals("g")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 1).trim(), isHex) * 1024 * 1024 * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 2).toLowerCase().equals("gb")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 2).trim(), isHex) * 1024 * 1024 * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 1).toLowerCase().equals("m")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 1).trim(), isHex) * 1024 * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 2).toLowerCase().equals("mb")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 2).trim(), isHex) * 1024 * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 1).toLowerCase().equals("k")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 1).trim(), isHex) * 1024;
			} catch (Exception ex) {
			}
		} else if (filesize.substring(filesize.length() - 2).toLowerCase().equals("kb")) {
			try {
				return Long.parseLong(filesize.substring(0, filesize.length() - 2).trim(), isHex) * 1024;
			} catch (Exception ex) {
			}
		} else {
			try {
				return Long.parseLong(filesize, isHex);
			} catch (Exception ex) {
			}
		}
		return returnValue;
	}

	public static void centerDialog(JFrame dialog) {
		// Center the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = dialog.getSize();

		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}

		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}

		dialog.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	}

	public static void centerDialog(JDialog dialog) {
		// Center the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = dialog.getSize();

		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}

		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}

		dialog.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	}

	public static void centerDialog(JFileChooser chooser) {
		// Center the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = chooser.getSize();

		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}

		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}

		chooser.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	}

	private static char numberOfFile_runningChar[] = {'-', '\\', '|', '/'};

	public static int numberOfFile(File node, boolean isSlient) {
		int x = 0;
		for (File file : node.listFiles()) {
			if (!isSlient) {
				System.out.print("\r" + numberOfFile_runningChar[x % numberOfFile_runningChar.length]);
			}
			if (file.isDirectory()) {
				x += numberOfFile(file, isSlient);
			} else {
				x++;
			}
		}
		System.out.print("\r \r");
		return x;
	}

	public static int getNumberOfFileAndDirectory(String file) {
		return getNumberOfFileAndDirectory(new File(file));
	}

	public static int getNumberOfFileAndDirectory(File file) {
		if (file.isFile()) {
			return -1;
		} else {
			File files[] = file.listFiles();
			int num = files.length;
			for (int x = 0; x < files.length; x++) {
				if (files[x].isDirectory()) {
					num += getNumberOfFileAndDirectory(files[x]);
				}
			}
			return num;
		}
	}

	public static int getNumberOfFile(String file) {
		return getNumberOfFile(new File(file));
	}

	public static int getNumberOfFile(File file) {
		if (file.isFile()) {
			return -1;
		} else {
			int num = 0;
			File files[] = file.listFiles();
			for (int x = 0; x < files.length; x++) {
				if (files[x].isDirectory()) {
					num += getNumberOfFile(files[x]);
				} else if (files[x].isFile()) {
					num++;
				}
			}
			return num;
		}
	}

	public static int getNumberOfFile(File file, String[] extensions) {
		if (file.isFile()) {
			return -1;
		} else {
			int num = 0;
			File files[] = file.listFiles();
			for (int x = 0; x < files.length; x++) {
				if (files[x].isDirectory()) {
					num += getNumberOfFile(files[x], extensions);
				} else if (files[x].isFile()) {
					boolean bingo = false;
					for (String ext : extensions) {
						if (files[x].getName().toLowerCase().endsWith(ext.toLowerCase())) {
							bingo = true;
							break;
						}
					}
					if (bingo) {
						//System.out.println(files[x]);
						num++;
					}
				}
			}
			return num;
		}
	}

	public static int getNumberOfDirectory(String file) {
		return getNumberOfDirectory(new File(file));
	}

	public static int getNumberOfDirectory(File file) {
		if (file.isFile()) {
			return -1;
		} else {
			int num = 0;
			File files[] = file.listFiles();
			for (int x = 0; x < files.length; x++) {
				if (files[x].isDirectory()) {
					num += getNumberOfDirectory(files[x]);
					num++;
				}
			}
			return num;
		}
	}

	public static byte[] readFile(File file) {
		return readFile(file.getAbsolutePath(), 0, (int) file.length());
	}

	public static byte readFileByte(File file, long offset) {
		return readFileByte(file.getAbsolutePath(), offset);
	}

	public static byte readFileByte(String filepath, long offset) {
		try {
			RandomAccessFile br = new RandomAccessFile(filepath, "r");
			br.seek(offset);
			byte b = br.readByte();
			br.close();
			return b;
		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	public static boolean deleteDirectory(String path) {
		if (new File(path).exists()) {
			File[] files = new File(path).listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i].getPath());
				} else {
					files[i].delete();
				}
			}
		}
		return (new File(path).delete());
	}

	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i].getPath());
				} else {
					files[i].delete();
				}
			}
		}
		return path.delete();
	}

	public static byte[] readFile(File file, long offset, int size) {
		return readFile(file.getAbsolutePath(), offset, size);
	}

	public static byte[] readFile(String filepath, long offset, int size) {
		try {
			RandomAccessFile br = new RandomAccessFile(filepath, "r");
			byte data[] = new byte[size];
			br.seek(offset);
			br.read(data);
			br.close();
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static int[] readFileUnsigned(File file, long offset, int size) {
		return readFileUnsigned(file.getAbsolutePath(), offset, size);
	}

	public static int[] readFileUnsigned(String filepath, long offset, int size) {
		try {
			RandomAccessFile br = new RandomAccessFile(filepath, "r");
			int data[] = new int[size];
			br.seek(offset);
			for (int x = 0; x < size; x++) {
				data[x] = br.readUnsignedByte();
			}
			br.close();
			return data;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static long readLongFromInputStream(DataInputStream in) throws IOException {
		long l = in.readUnsignedByte() + (in.readUnsignedByte() << 8) + (in.readUnsignedByte() << 16) + (in.readUnsignedByte() << 24);
		l &= 0xffffffffL;
		return l;
	}

	public static long readLong64BitsFromInputStream(DataInputStream in) throws IOException {
		long l = in.readUnsignedByte() + (in.readUnsignedByte() << 8) + (in.readUnsignedByte() << 16) + (in.readUnsignedByte() << 24) + (in.readUnsignedByte() << 32)
				+ (in.readUnsignedByte() << 40) + (in.readUnsignedByte() << 48) + (in.readUnsignedByte() << 56);
		l &= 0xffffffffffffffffL;
		return l;
	}

	public static long readShortFromInputStream(DataInputStream in) throws IOException {
		long l = in.readUnsignedByte() + (in.readUnsignedByte() << 8);
		l &= 0xffffffffL;
		return l;
	}

	public static Object[] reverseArray(Object objects[]) {
		try {
			List l = Arrays.asList(objects);
			java.util.Collections.reverse(l);
			/**
			 * and if you must back to the array again
			 */
			objects = l.toArray();
			return objects;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static String convertFilesize(long filesize) {
		if (filesize < 1024) {
			return filesize + " bytes";
		} else if (filesize >= 1024 * 1024 * 1024) {
			return filesize / 1024 / 1024 / 1024 + " GB";
		} else if (filesize >= 1024 && filesize < 1024 * 1024) {
			return filesize / 1024 + " KB";
		} else {
			return filesize / 1024 / 1024 + " MB";
		}
	}

	public static void printSecond(long second) {
		if (second > 60 * 60 * 24 * 365) {
			System.out.println(second / (60 * 60 * 24 * 365) + " years, " + second + " seconds");
		} else if (second > 60 * 60 * 24) {
			System.out.println(second / (60 * 60 * 24) + " days, " + second + " seconds");
		} else if (second > 60 * 60) {
			System.out.println(second / (60 * 60) + " hours, " + second + " seconds");
		} else if (second > 60) {
			System.out.println(second / (60) + " minutes, " + second + " seconds");
		} else {
			System.out.println(second + " seconds");
		}
	}

	public static String getRelativePath(File file) {
		return getRelativePath(file.getAbsolutePath());
	}

	public static String getRelativePath(String file) {
		return file.substring(System.getProperty("user.dir").length());
	}

	public static void highlightUsingRegularExpression(JTextComponent textComp, String regularExpression, Color color) {
		removeHighlights(textComp);
		Highlighter.HighlightPainter myHighlightPainter = new MyHighlightPainter(color);

		try {
			Highlighter hilite = textComp.getHighlighter();
			Document doc = textComp.getDocument();
			String text = doc.getText(0, doc.getLength());
			int pos = 0;

			String inputStr = textComp.getText();
			Pattern pattern = Pattern.compile(regularExpression);
			Matcher matcher = pattern.matcher(inputStr);
			boolean matchFound = matcher.find();
			while (matchFound) {
				// System.out.println(matcher.start() + "-" + matcher.end());
				for (int i = 0; i <= matcher.groupCount(); i++) {
					String groupStr = matcher.group(i);
					// System.out.println(i + ":" + groupStr);
				}
				hilite.addHighlight(matcher.start(), matcher.end(), myHighlightPainter);
				if (matcher.end() + 1 <= inputStr.length()) {
					matchFound = matcher.find(matcher.end());
				} else {
					break;
				}
			}
		} catch (BadLocationException e) {
		}
	}

	public static void highlight(JTextComponent textComp, String pattern, Color color) {
		// First remove all old highlights
		removeHighlights(textComp);
		Highlighter.HighlightPainter myHighlightPainter = new MyHighlightPainter(color);

		try {
			Highlighter hilite = textComp.getHighlighter();
			Document doc = textComp.getDocument();
			String text = doc.getText(0, doc.getLength());
			int pos = 0;

			// Search for pattern
			while ((pos = text.indexOf(pattern, pos)) >= 0) {
				// Create highlighter using private painter and apply around
				// pattern
				hilite.addHighlight(pos, pos + pattern.length(), myHighlightPainter);
				pos += pattern.length();
			}
		} catch (BadLocationException e) {
		}
	}

	// Removes only our private highlights
	public static void removeHighlights(JTextComponent textComp) {
		Highlighter hilite = textComp.getHighlighter();
		Highlighter.Highlight[] hilites = hilite.getHighlights();

		for (int i = 0; i < hilites.length; i++) {
			if (hilites[i].getPainter() instanceof MyHighlightPainter) {
				hilite.removeHighlight(hilites[i]);
			}
		}
	}

	public static void printFilesize(long fileOffset, long filesize) {
		System.out.print("\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r");
		if (fileOffset < 1024) {
			System.out.print(fileOffset + " /" + filesize + " bytes, " + fileOffset * 100 / filesize + "%            ");
		} else if (fileOffset >= 1024 & fileOffset < 1024 * 1024) {
			System.out.print(fileOffset / 1024 + " /" + filesize / 1024 + " KB, " + fileOffset * 100 / filesize + "%          ");
		} else if (fileOffset >= 1024 * 1024) {
			System.out.print(fileOffset / 1024 / 1024 + "MB (" + fileOffset / 1024 + " KB) / " + filesize / 1024 / 1024 + " MB, " + fileOffset * 100 / filesize + "%         ");
		}
	}

	public static String trimByteZero(String str) {
		if (str.indexOf(0) == -1) {
			return str;
		} else {
			return str.substring(0, str.indexOf(0));
		}
	}

	public static Image iconToImage(Icon icon) {
		if (icon instanceof ImageIcon) {
			return ((ImageIcon) icon).getImage();
		} else {
			int w = icon.getIconWidth();
			int h = icon.getIconHeight();
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gd = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gd.getDefaultConfiguration();
			BufferedImage image = gc.createCompatibleImage(w, h);
			Graphics2D g = image.createGraphics();
			icon.paintIcon(null, g, 0, 0);
			g.dispose();
			return image;

		}
	}

	public static void drawDot(Graphics g, int x, int y) {
		g.drawLine(x, y, x, y);
	}

	public static void skip(InputStream inputStream, long offset) throws IOException {
		long l = offset;

		do {
			long temp = inputStream.skip(l);
//			System.out.println(l + " = " + temp);
			l -= temp;
		} while (l != 0);
	}

	public static long read(InputStream inputStream, int noOfByte) throws IOException {
		byte bytes[] = new byte[noOfByte];
		inputStream.read(bytes);
		return getInt(bytes);
	}

	static class MyHighlightPainter extends DefaultHighlighter.DefaultHighlightPainter {

		public MyHighlightPainter(Color color) {
			super(color);
		}
	}

	public static void writeFile(String filepath, String content) {
		try {

			FileWriter fstream = new FileWriter(filepath);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(content);
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void appendFile(String filepath, String content) {
		try {
			FileOutputStream file = new FileOutputStream(filepath, true);
			DataOutputStream out = new DataOutputStream(file);
			out.writeBytes(content);
			out.flush();
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String empty(Object str) {
		if (str == null) {
			return "";
		} else {
			return str.toString();
		}
	}

	public static String runCommand(String command) throws Exception {
		return runCommand(command, 0);
	}

	public static String runCommand(String command, int skipLine) throws Exception {
		StringBuilder sb = new StringBuilder(4096);
		int x = 0;
		String s;
		Process p = Runtime.getRuntime().exec(command);

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		// read the output from the command
		while ((s = stdInput.readLine()) != null) {
			if (x >= skipLine) {
				sb.append(s);
				sb.append(System.getProperty("line.separator"));
			}
			x++;
		}
		stdInput.close();
		return sb.toString();
	}

	public static void captureComponentToJpeg(Component c, File destFile) throws IOException {
		BufferedImage image = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_RGB);
		c.paint(image.createGraphics());
		ImageIO.write(image, "JPEG", destFile);
	}

	public static String getWebpage(String url) {
		String content = "";
		if (!url.trim().toLowerCase().startsWith("http://")) {
			url = "http://" + url;
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				content += line + "\n";
			}

			reader.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		return content;
	}

	public static Enumeration makeEnumeration(final Object obj) {
		Class type = obj.getClass();
		if (!type.isArray()) {
			throw new IllegalArgumentException(obj.getClass().toString());
		} else {
			return (new Enumeration() {
				int size = Array.getLength(obj);

				int cursor;

				public boolean hasMoreElements() {
					return (cursor < size);
				}

				public Object nextElement() {
					return Array.get(obj, cursor++);
				}
			});
		}
	}

	public static void expandAll(JTree tree, boolean expand) {
		expandAll(tree, expand, -1);
	}

	public static void expandAll(JTree tree, boolean expand, int maxLevel) {
		TreeNode root = (TreeNode) tree.getModel().getRoot();
		if (root != null) {
			expandAll(tree, new TreePath(root), expand, maxLevel, 0);
		}
	}

	private static void expandAll(JTree tree, TreePath treePath, boolean expand, int maxLevel, int currentLevel) {
		if (maxLevel != -1 && currentLevel >= maxLevel - 1) {
			return;
		}

		TreeNode node = (TreeNode) treePath.getLastPathComponent();
		// if (node instanceof FilterTreeNode) {
		// FilterTreeNode filterTreeNode = (FilterTreeNode) node;
		// if (!filterTreeNode.isShown) {
		// return;
		// }
		// }
		if (node.getChildCount() >= 0) {
			for (Enumeration e = node.children(); e.hasMoreElements();) {
				TreeNode n = (TreeNode) e.nextElement();

				TreePath path = treePath.pathByAddingChild(n);
				expandAll(tree, path, expand, maxLevel, currentLevel + 1);
			}
		}

		// Expansion or collapse must be done bottom-up
		if (expand) {
			tree.expandPath(treePath);
		} else {
			tree.collapsePath(treePath);
		}
	}

	public static TreePath findTreeNode(TreeNode node, String pattern, TreePath treePath) {
		Enumeration e = node.children();
		while (e.hasMoreElements()) {
			TreeNode obj = (TreeNode) e.nextElement();
			if (obj.toString().equals(pattern)) {
				return treePath.pathByAddingChild(obj);
			}
			if (!obj.isLeaf()) {
				return findTreeNode(obj, pattern, treePath.pathByAddingChild(obj));
			}
		}
		return null;
	}

	public static void saveFile(String str, File file) {
		try {
			// Create file
			FileWriter fstream = new FileWriter(file);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(str);
			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			e.printStackTrace();
		}
	}

	public static void writeFile(InputStream is, File file) {
		if (is == null || file == null) {
			return;
		}
		BufferedOutputStream fOut = null;
		try {
			fOut = new BufferedOutputStream(new FileOutputStream(file));
			byte[] buffer = new byte[32 * 1024];
			int bytesRead = 0;
			while ((bytesRead = is.read(buffer)) != -1) {
				fOut.write(buffer, 0, bytesRead);
			}
			fOut.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static byte[] hexStringToByteArray(String s) {
		if (s.length() % 2 == 1) {
			s = "0" + s;
		}
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public static byte[] integerStringToByteArray(String s) {
		if (s.length() % 2 == 1) {
			s = "0" + s;
		}
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 10) * 10) + Character.digit(s.charAt(i + 1), 10));
		}
		return data;
	}

	public static byte[] stringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len];
		for (int i = 0; i < len; i++) {
			data[i] = (byte) s.charAt(i);
		}
		return data;
	}

	public static void putShort(int b[], short s, int index) {
		b[index] = (byte) (s >> 0);
		b[index + 1] = (byte) (s >> 8);
	}

	public static long getShort(int b0, int b1) {
		return (long) ((b0 & 0xffL << 0) | (b1 & 0xffL) << 8);
	}

	public static long getShort(int[] bb, int index) {
		return (long) (((bb[index + 0] & 0xffL) << 0) | ((bb[index + 1] & 0xffL) << 8));
	}

	// ///////////////////////////////////////////////////////
	public static void putInt(int[] bb, int x, int index) {
		bb[index + 0] = (byte) (x >> 0);
		bb[index + 1] = (byte) (x >> 8);
		bb[index + 2] = (byte) (x >> 16);
		bb[index + 3] = (byte) (x >> 24);
	}

	public static long getInt(byte[] bb, int index) {
		return getInt(bb[index], bb[index + 1], bb[index + 2], bb[index + 3]);
	}

	public static long getInt(byte b0, byte b1, byte b2, byte b3) {
		return (long) ((((b0 & 0xffL) << 0) | ((b1 & 0xffL) << 8) | ((b2 & 0xffL) << 16) | ((b3 & 0xffL) << 24)));
	}

	public static long getInt(int[] bb, int index) {
		return getInt(bb[index], bb[index + 1], bb[index + 2], bb[index + 3]);
	}

	public static long getInt(int b0, int b1, int b2, int b3) {
		return (long) ((((b0 & 0xffL) << 0) | ((b1 & 0xffL) << 8) | ((b2 & 0xffL) << 16) | ((b3 & 0xffL) << 24)));
	}

	public static long getInt(byte[] bb) {
		long l = 0;
		int x = 0;
		for (byte b : bb) {
			l += (b & 0xff) << x;
			x += 8;
		}
		return l;
	}

	public static BigInteger get64BitsInt(ByteBuffer bufferBuffer) {
		byte tempBytes[] = new byte[8];
		bufferBuffer.get(tempBytes);
		return new BigInteger(1, tempBytes);
	}

	public static long get64BitsInt(byte[] bb, int index) {
		return (long) ((((bb[index + 0] & 0xffL) << 0) | ((bb[index + 1] & 0xffL) << 8) | ((bb[index + 2] & 0xffL) << 16) | ((bb[index + 3] & 0xffL) << 24)
				| ((bb[index + 4] & 0xffL) << 32) | ((bb[index + 5] & 0xffL) << 40) | ((bb[index + 6] & 0xffL) << 48) | ((bb[index + 7] & 0xffL) << 56)));
	}

	public static long get64BitsInt(byte b0, byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7) {
		return (long) ((((b0 & 0xffL) << 0) | ((b1 & 0xffL) << 8) | ((b2 & 0xffL) << 16) | ((b3 & 0xffL) << 24)) | ((b4 & 0xffL) << 32) | ((b5 & 0xffL) << 40)
				| ((b6 & 0xffL) << 48) | ((b7 & 0xffL) << 56));
	}

	public static long get64BitsInt(int[] bb, int index) {
		return (long) ((((bb[index + 0] & 0xffL) << 0) | ((bb[index + 1] & 0xffL) << 8) | ((bb[index + 2] & 0xffL) << 16) | ((bb[index + 3] & 0xffL) << 24)
				| ((bb[index + 4] & 0xffL) << 32) | ((bb[index + 5] & 0xffL) << 40) | ((bb[index + 6] & 0xffL) << 48) | ((bb[index + 7] & 0xffL) << 56)));
	}

	public static long get64BitsInt(int b0, int b1, int b2, int b3, int b4, int b5, int b6, int b7) {
		return (long) ((((b0 & 0xffL) << 0) | ((b1 & 0xffL) << 8) | ((b2 & 0xffL) << 16) | ((b3 & 0xffL) << 24)) | ((b4 & 0xffL) << 32) | ((b5 & 0xffL) << 40)
				| ((b6 & 0xffL) << 48) | ((b7 & 0xffL) << 56));
	}

	public static BigInteger getBigInteger(BigInteger[] bb, int index) {
		BigInteger bi = bb[0];
		bi = bi.add(bb[1].shiftLeft(8));
		bi = bi.add(bb[2].shiftLeft(16));
		bi = bi.add(bb[3].shiftLeft(24));
		return bi;
	}

	public static BigInteger getBigInteger(int[] bb, int index) {
		BigInteger bi = BigInteger.valueOf(bb[index] & 0xffL);
		bi = bi.add(BigInteger.valueOf(bb[index + 1] & 0xffL).shiftLeft(8));
		bi = bi.add(BigInteger.valueOf(bb[index + 2] & 0xffL).shiftLeft(16));
		bi = bi.add(BigInteger.valueOf(bb[index + 3] & 0xffL).shiftLeft(24));
		return bi;
	}

	public static BigInteger getBigInteger(int b0, int b1, int b2, int b3) {
		BigInteger bi = BigInteger.valueOf(b0 & 0xffL);
		bi = bi.add(BigInteger.valueOf(b1 & 0xffL).shiftLeft(8));
		bi = bi.add(BigInteger.valueOf(b2 & 0xffL).shiftLeft(16));
		bi = bi.add(BigInteger.valueOf(b3 & 0xffL).shiftLeft(24));
		return bi;
	}

	public static void putLong(int[] bb, long x, int index) {
		bb[index + 0] = (byte) (x >> 0);
		bb[index + 1] = (byte) (x >> 8);
		bb[index + 2] = (byte) (x >> 16);
		bb[index + 3] = (byte) (x >> 24);
		bb[index + 4] = (byte) (x >> 32);
		bb[index + 5] = (byte) (x >> 40);
		bb[index + 6] = (byte) (x >> 48);
		bb[index + 7] = (byte) (x >> 56);
	}

	public static long getLong(int b0, byte b1) {
		return (long) (b0 & 0xffL) | (long) (b1 & 0xffL) << 8;
	}

	public static long getLong(int[] bb, int index) {
		return (((long) bb[index + 0] & 0xffL) | (((long) bb[index + 1] & 0xffL) << 8) | (((long) bb[index + 2] & 0xffL) << 16) | (((long) bb[index + 3] & 0xffL) << 24)
				| (((long) bb[index + 4] & 0xffL) << 32) | (((long) bb[index + 5] & 0xffL) << 40) | (((long) bb[index + 6] & 0xffL) << 48)
				| (((long) bb[index + 7] & 0xffL) << 56));
	}

	public static long getLong(int b0, int b1, int b2, int b3, int b4, int b5, int b6, int b7) {
		return (((long) b0 & 0xffL) | (((long) b1 & 0xffL) << 8) | (((long) b2 & 0xffL) << 16) | (((long) b3 & 0xffL) << 24) | (((long) b4 & 0xffL) << 32)
				| (((long) b5 & 0xffL) << 40) | (((long) b6 & 0xffL) << 48) | (((long) b7 & 0xffL) << 56));
	}

	public static BigInteger getBigInteger(int b0, int b1, int b2, int b3, int b4, int b5, int b6, int b7) {
		BigInteger bi = BigInteger.valueOf(b0 & 0xffL);
		bi = bi.add(BigInteger.valueOf(b1 & 0xffL).shiftLeft(8));
		bi = bi.add(BigInteger.valueOf(b2 & 0xffL).shiftLeft(16));
		bi = bi.add(BigInteger.valueOf(b3 & 0xffL).shiftLeft(24));
		bi = bi.add(BigInteger.valueOf(b4 & 0xffL).shiftLeft(32));
		bi = bi.add(BigInteger.valueOf(b5 & 0xffL).shiftLeft(40));
		bi = bi.add(BigInteger.valueOf(b6 & 0xffL).shiftLeft(48));
		bi = bi.add(BigInteger.valueOf(b7 & 0xffL).shiftLeft(56));
		return bi;
	}

	public static long getBit(long value, int bitNo) {
		return value >> bitNo & 1;
	}

	public static long getBit(BigInteger value, int bitNo) {
		return value.testBit(bitNo) ? 1 : 0;
	}

	public static long getValue(long l, int startBit, int endBit) {
		if (startBit > endBit) {
			int temp = startBit;
			startBit = endBit;
			endBit = temp;
		}
		l = l >> startBit;
		return l & new Double(Math.pow(2, (endBit - startBit + 1)) - 1).longValue();
	}

	public static BigInteger getBigInteger(long longVar, int startBit, int endBit) {
		if (startBit > endBit) {
			int temp = startBit;
			startBit = endBit;
			endBit = temp;
		}
		BigInteger l = BigInteger.valueOf(longVar);
		l = l.shiftRight(startBit);
		l = l.and(BigInteger.valueOf(new Double(Math.pow(2, (endBit - startBit + 1)) - 1).longValue()));
		return l;
	}

	public static BigInteger getBigInteger(BigInteger l, int startBit, int endBit) {
		if (startBit > endBit) {
			int temp = startBit;
			startBit = endBit;
			endBit = temp;
		}
		l = l.shiftRight(startBit);
		l = l.and(BigInteger.valueOf(new Double(Math.pow(2, (endBit - startBit + 1)) - 1).longValue()));
		return l;
	}

	public static int[] byteArrayToIntArray(byte[] b) {
		int i[] = new int[b.length];
		for (int x = 0; x < i.length; x++) {
			i[x] = b[x];
		}
		return i;
	}

	public static byte[] intArrayToByteArray(int[] b) {
		if (b == null) {
			return null;
		}
		byte i[] = new byte[b.length];
		for (int x = 0; x < i.length; x++) {
			i[x] = (byte) b[x];
		}
		return i;
	}

	public static int[] getBytes(int value) {
		int b[] = new int[4];
		for (int x = 0; x < 4; x++) {
			b[x] = (int) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static int[] getBytes(long value) {
		int b[] = new int[8];
		for (int x = 0; x < 8; x++) {
			b[x] = (int) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static byte[] getByteArray(int value) {
		byte b[] = new byte[4];
		for (int x = 0; x < 4; x++) {
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static byte[] getByteArray(long value) {
		byte b[] = new byte[8];
		for (int x = 0; x < 8; x++) {
			b[x] = (byte) ((value >> (x * 8)) & 0xffL);
		}
		return b;
	}

	public static int getFileSize(String url) {
		try {
			return getFileSize(new URL(url));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return -1;
	}

	public static int getFileSize(URL url) {
		try {
			int size;
			URLConnection conn = url.openConnection();
			size = conn.getContentLength();
			return size;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public static String splitString(String str, String delim, int size) {
		String ori = str;
		String s = "";
		for (int x = 0; x < ori.length(); x += size) {
			if (x + size < ori.length()) {
				s += ori.substring(x, x + size) + delim;
			} else {
				s += ori.substring(x, ori.length()) + delim;
			}
		}
		return s;
	}

	public static boolean portIsOpen(String ip, int port, int timeout) {
		try {
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port), timeout);
			socket.close();
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static String arrayToHexString(byte arr[]) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		String r = "";
		for (int x = 0; x < arr.length; x++) {
			if (!r.equals("")) {
				r += " ";
			}
			r += "0x" + String.format("%02X", arr[x] & 0xff);
		}
		return r;
	}

	public static String arrayToHexString(int arr[]) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		String r = "";
		for (int x = 0; x < arr.length; x++) {
			if (!r.equals("")) {
				r += " ";
			}
			r += "0x" + String.format("%02X", arr[x]);
		}
		return r;
	}

	public static String arrayToHexStringReverse(byte arr[]) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		String r = "";
		for (int x = arr.length - 1; x >= 0; x--) {
			if (!r.equals("")) {
				r += " ";
			}
			r += "0x" + String.format("%02X", arr[x] & 0xff);
		}
		return r;
	}

	public static String arrayToHexStringReverse(byte arr[], int noOfBytesToSwap) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		String r = "";
		int x = 0;
		while (x < arr.length) {
			for (int z = 0; z < noOfBytesToSwap; z++) {
				r += "0x" + String.format("%02X", arr[x + noOfBytesToSwap - z - 1]) + " ";
			}
			x += noOfBytesToSwap;
		}
		return r.trim();
	}

	public static String arrayToHexStringReverse(int arr[]) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		String r = "";
		for (int x = arr.length - 1; x >= 0; x--) {
			if (!r.equals("")) {
				r += " ";
			}
			r += "0x" + String.format("%02X", arr[x]);
		}
		return r;
	}

	// public static BufferedImage brighter(BufferedImage img) {
	// return toBufferedImage(img);
	// }
	// public static BufferedImage brighter(Image img) {
	// float[] factors = new float[] { 1.4f, 1.4f, 1.4f };
	// float[] offsets = new float[] { 0.0f, 0.0f, 0.0f };
	// RescaleOp rescaleOp = new RescaleOp(1.4f, 0.0f, null);
	// return rescaleOp.filter(toBufferedImage(img), null);
	// }
	public static BufferedImage brighter(Image image) {
		BufferedImage img = toBufferedImage(image);
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D bGr = bimage.createGraphics();
		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				bGr.setColor(new Color(img.getRGB(x, y)).brighter());
				bGr.fillRect(x, y, 1, 1);
			}
		}
		bGr.dispose();
		return bimage;
	}

	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();
		return bimage;
	}

	public static BufferedImage toBufferedImage(ImageIcon imageIcon) {
		Image img = imageIcon.getImage();
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();
		return bimage;
	}

	public static BufferedImage resizeImage(BufferedImage originalImage, int width, int height, int type) throws IOException {
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		return resizedImage;
	}

	public static BufferedImage resizeImage(ImageIcon imageIcon, int width, int height, int type) throws IOException {
		BufferedImage originalImage = toBufferedImage(imageIcon);
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		return resizedImage;
	}

	public static BufferedImage resizeImageByHeight(BufferedImage originalImage, int preferredHeight, int type) {
		try {
			int width = originalImage.getWidth() * preferredHeight / originalImage.getHeight();
			BufferedImage resizedImage = new BufferedImage(width, preferredHeight, type);
			Graphics2D g = resizedImage.createGraphics();
			g.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
			g.drawImage(originalImage, 0, 0, width, preferredHeight, null);
			g.dispose();
			return resizedImage;
		} catch (Exception ex) {
			return null;
		}
	}

	public static ImageIcon resizeImageToIcon(ImageIcon icon, int width, int height, int mode) {
		return new ImageIcon(icon.getImage().getScaledInstance(width, height, mode));
	}

	public static void resizeColumnWidth(JTable table) {
		resizeColumnWidth(table, 50, 0);
	}

	public static void resizeColumnWidth(JTable table, int minWidth, int margin) {
		final TableColumnModel columnModel = table.getColumnModel();
		for (int column = 0; column < table.getColumnCount(); column++) {
			int temp = minWidth;
			for (int row = 0; row < table.getRowCount(); row++) {
				TableCellRenderer renderer = table.getCellRenderer(row, column);
				Component comp = table.prepareRenderer(renderer, row, column);
				temp = Math.max(comp.getPreferredSize().width, temp);
			}
			columnModel.getColumn(column).setPreferredWidth(temp + margin);
		}
	}

	public static void resizeRowHeight(JTable table, int height) {
		for (int row = 0; row < table.getRowCount(); row++) {
			table.setRowHeight(height);
		}
	}

	public static ImageIcon resizeImageByHeight(BufferedImage b, int preferredHeight) {
		try {
			int width = b.getWidth() * preferredHeight / b.getHeight();
			Image i = b.getScaledInstance(width, preferredHeight, Image.SCALE_SMOOTH);
			return new ImageIcon(i);
		} catch (Exception ex) {
			return null;
		}
	}

	public static boolean isDisable(JComponent c) {
		boolean disable = false;
		if (c.getClientProperty("disable") != null) {
			disable = (boolean) c.getClientProperty("disable");
		}
		return disable;
	}

	public static void enableJComponent(JComponent c, boolean isEnable) {
		enableJComponent(c, isEnable, new ArrayList<JComponent>());
	}

	private static void enableJComponent(JComponent c, boolean isEnable, ArrayList<JComponent> map) {
		if (map.contains(c)) {
			return;
		}
		map.add(c);

		c.setEnabled(isEnable);
		c.putClientProperty("disable", !isEnable);
		for (Component component : c.getComponents()) {
			if (component instanceof JComponent) {
				JComponent jcomponent = ((JComponent) component);
				enableJComponent(jcomponent, isEnable);
			}
		}
	}

	public static String getHexString(byte[] b, String separator) {
		return getHexString(b, separator, true);
	}

	public static String getHexString(byte[] b, String separator, boolean print0x) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			if (print0x) {
				result += "0x" + Integer.toString(b[i] & 0xff, 16) + separator;
			} else {
				result += Integer.toString(b[i] & 0xff, 16) + separator;
			}
		}
		return result;
	}

	public static String getHexString(int[] b, String separator) {
		String result = "";
		for (int i = 0; i < b.length; i++) {
			result += "0x" + Integer.toString(b[i], 16) + separator;
		}
		return result;
	}

	public static int[] unsignedByteArray(byte[] block) {
		int b[] = new int[block.length];
		for (int x = 0; x < block.length; x++) {
			b[x] = block[x] & 0xff;
		}
		return b;
	}

	public static String printException(Exception ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public static void highlightTextField(JTextField textField) {
		textField.putClientProperty("hightlightRed", true);
		textField.repaint();
	}

	public static void disableHighlightTextField(JTextField textField) {
		textField.putClientProperty("hightlightRed", null);
		textField.repaint();
	}

	public static int getNumberOfNodes(TreeModel model) {
		return getNumberOfNodes(model, model.getRoot());
	}

	private static int getNumberOfNodes(TreeModel model, Object node) {
		// System.out.println("getNumberOfNodes=" + node);
		int count = 1;
		int nChildren = model.getChildCount(node);
		for (int i = 0; i < nChildren; i++) {
			count += getNumberOfNodes(model, model.getChild(node, i));
		}
		return count;
	}

	public static Color getRandomColor() {
		Random random = new Random();
		return new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
	}

	public static String getRandomString(int length) {
		final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
		StringBuilder result = new StringBuilder();
		while (length > 0) {
			Random rand = new Random();
			result.append(characters.charAt(rand.nextInt(characters.length())));
			length--;
		}
		return result.toString();
	}

	public static Color getRandomColor(String str) {
		int hash = str.hashCode();
		int r = (hash & 0xFF0000) >> 16;
		int g = (hash & 0x00FF00) >> 8;
		int b = hash & 0x0000FF;
		return new Color(r, g, b);
	}

	public static Color getLightColor(String str, float brightness) {
		if (str == null) {
			str = "";
		}
		int iHash = Math.abs(str.hashCode());
		String sHash = "0." + iHash;
		Float dHash = Float.valueOf(sHash);
		return Color.getHSBColor(dHash, 0.5f, brightness);
	}

	public static Color darker(Color color, float f) {
		return new Color(color.getRed() * f / 255, color.getGreen() * f / 255, color.getBlue() * f / 255, color.getAlpha() / 255);
	}

	public static String toHexColor(Color color) {
		return "#" + Integer.toHexString(color.getRGB()).substring(2);
	}

	public static String randomString(int count, boolean numberic, boolean alphabetic, boolean caseSensitive) throws OptionErrorException {
		if (numberic && alphabetic && caseSensitive) {
			return RandomStringUtils.randomAlphanumeric(count); // random number, upper and lower case
		} else if (numberic && alphabetic && !caseSensitive) {
			return (RandomStringUtils.random(count, true, true)).toUpperCase(); // random(count, letters, numbers)
		} else if (!numberic && alphabetic && caseSensitive) {
			return RandomStringUtils.randomAlphabetic(count); // random upper and lower case
		} else if (!numberic && alphabetic && !caseSensitive) {
			return (RandomStringUtils.random(count, true, false)).toUpperCase(); // random(count, letters, numbers)
		} else if (numberic && !alphabetic && !caseSensitive) {
			return (RandomStringUtils.randomNumeric(count)).toUpperCase(); // random number
		} else {
			throw new OptionErrorException("Options Error");
		}
	}

	public static BufferedImage stringToBufferedImage(String s, String fontStyle, int fontSize, Color color) {
		BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = img.getGraphics();

		Font f = new Font(fontStyle, Font.PLAIN, fontSize);
		g.setFont(f);

		FontRenderContext frc = g.getFontMetrics().getFontRenderContext();
		Rectangle2D rect = f.getStringBounds(s, frc);
		g.dispose();

		final int paddingLeftRight = 3;
		img = new BufferedImage((int) Math.ceil(rect.getWidth()) + paddingLeftRight * 2, (int) Math.ceil(rect.getHeight()), BufferedImage.TYPE_4BYTE_ABGR);
		g = img.getGraphics();
		g.setColor(color);
		g.setFont(f);

		FontMetrics fm = g.getFontMetrics();
		int x = 0;
		int y = fm.getAscent();
		g.drawString(s, x + paddingLeftRight, y);
		g.dispose();

		return img;
	}

	public static void paintBackgorund(Graphics g, Image bg, int width, int height) {
		for (int y = 0; y < height; y += bg.getHeight(null)) {
			for (int x = 0; x < width; x += bg.getWidth(null)) {
				g.drawImage(bg, x, y, null);
			}
		}
	}

	public static void scrollTableToVisible(JTable table, int rowIndex, int vColIndex) {
		table.scrollRectToVisible(new Rectangle(table.getCellRect(rowIndex, vColIndex, true)));
	}

	public static String prettyFormatXml(String xml, int indent) {
		try {
			Source xmlInput = new StreamSource(new StringReader(xml));
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", indent);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(xmlInput, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (Exception e) {
			throw new RuntimeException(e); // simple exception handling, please review it
		}
	}

	public static String prettyFormatJson(String json) {
		JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		JsonElement el = parser.parse(json);
		String jsonString = gson.toJson(el);
		return jsonString;
	}

	public static void autoResizeColumn(JTable table) {
		final TableColumnModel columnModel = table.getColumnModel();
		for (int column = 0; column < table.getColumnCount(); column++) {
			int width = 15;
			for (int row = 0; row < table.getRowCount(); row++) {
				TableCellRenderer renderer = table.getCellRenderer(row, column);
				Component comp = table.prepareRenderer(renderer, row, column);
				width = Math.max(comp.getPreferredSize().width + 1, width);
			}
			width += 10;
			columnModel.getColumn(column).setPreferredWidth(width);
		}
	}

	public static String formatJson(String jsonString) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(jsonString, Object.class);
			String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			return indented;
		} catch (IOException ex) {
			ex.printStackTrace(System.out);
			return null;
		}
	}

	public static boolean isImage(File file) {
		boolean isImage = true;
		try {
			Image image = ImageIO.read(file);
			if (image == null) {
				isImage = false;
			}
		} catch (IOException ex) {
			isImage = false;
		}
		return isImage;
	}

	public static String objectToJson(Object obj) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(obj);
		return formatJson(json);
	}

	public enum OSType {
		mac, win, linux
	};

	public static OSType getOS() {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.toLowerCase().contains("windows")) {
			return OSType.win;
		} else if (osName.toLowerCase().contains("mac")) {
			return OSType.mac;
		} else {
			return OSType.linux;
		}
	}
}

package hk.quantr.peterswing;

import hk.quantr.peterswing.FilterTreeNode;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

public class FilterTreeModel extends DefaultTreeModel {

	public String filter;
	public int maxFilterLevel = -1;
	private TreeModel treeModel;
	public boolean exactMatch;
	public boolean showAllChildNode;
	final int maxPoolSize = 20;
	ExecutorService pool;
	static int totalCount;
	public boolean multiThreadSearch;

	public FilterTreeModel(TreeModel delegate) {
		super((TreeNode) delegate.getRoot());
		this.treeModel = delegate;
	}

	public FilterTreeModel(TreeModel delegate, int maxFilterLevel) {
		this(delegate);
		this.maxFilterLevel = maxFilterLevel;
	}

	public FilterTreeModel(TreeModel delegate, int maxFilterLevel, boolean showAllChildNode) {
		this(delegate);
		this.maxFilterLevel = maxFilterLevel;
		this.showAllChildNode = showAllChildNode;
	}

	public void reload() {
		if (filter == null || filter.equals("")) {
			setAllChildVisible((FilterTreeNode) root, true);
			nodeStructureChanged((TreeNode) root);
			return;
		}
		FilterTreeNode root = (FilterTreeNode) getRoot();
		setAllChildVisible((FilterTreeNode) root, false);
		walk(root, 0);
		nodeStructureChanged((TreeNode) root);
	}

	void walk(final FilterTreeNode filterTreeNode, final int level) {
		//		final FilterTreeNode filterTreeNode = (FilterTreeNode) o;
		String nodeStringLowerCase = filterTreeNode.toString().toLowerCase();
		String filterLowerCase = filter.toLowerCase();
		if (exactMatch && nodeStringLowerCase.equals(filterLowerCase)) {
			setAllParentVisible(filterTreeNode, true);
			if (showAllChildNode) {
				setAllChildVisible(filterTreeNode, true);
			}
			if (filterTreeNode.isLeaf()) {
				showAllNodeInSameLevel(filterTreeNode, true);
			}
			return;
		} else if (!exactMatch && nodeStringLowerCase.contains(filterLowerCase)) {
			setAllParentVisible(filterTreeNode, true);
			if (showAllChildNode) {
				setAllChildVisible(filterTreeNode, true);
			}
			if (filterTreeNode.isLeaf() && !filterTreeNode.getParent().toString().equals(".debug_loc")) {
				showAllNodeInSameLevel(filterTreeNode, true);
			}
			return;
		}

		int count = filterTreeNode.getChildCount();
		if (multiThreadSearch && level == 1) {
			pool = Executors.newFixedThreadPool(maxPoolSize);
			for (int i = 0; i < count; i++) {
				final FilterTreeNode child = (FilterTreeNode) filterTreeNode.getChildAt(i);
				pool.execute(new Runnable() {
					public void run() {
						if (maxFilterLevel == -1 || level < maxFilterLevel - 1) {
							walk(child, level + 1);
						}
					}
				});
			}
			pool.shutdown();
			try {
				pool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			for (int i = 0; i < count; i++) {
				FilterTreeNode child = (FilterTreeNode) filterTreeNode.getChildAt(i);
				if (maxFilterLevel == -1 || level < maxFilterLevel - 1) {
					walk(child, level + 1);
				}
			}
		}
	}

	private void showAllNodeInSameLevel(FilterTreeNode filterTreeNode, boolean b) {
		if (filterTreeNode.getParent() == null) {
			return;
		}
		Enumeration<FilterTreeNode> e = (Enumeration<FilterTreeNode>) filterTreeNode.getParent().children();
		while (e.hasMoreElements()) {
			FilterTreeNode childNode = e.nextElement();
			childNode.isShown = b;
			setAllChildVisible(childNode, b);
		}
	}

	void setAllParentVisible(FilterTreeNode node, boolean b) {
		if (node.isShown == b) {
			return;
		}
		node.isShown = b;
		TreeNode parent = node.getParent();
		if (parent != null && parent instanceof FilterTreeNode) {
			setAllParentVisible((FilterTreeNode) parent, b);
		}
	}

	public void setAllChildVisible(FilterTreeNode node, boolean b) {
		Enumeration<TreeNode> e = node.children();
		while (e.hasMoreElements()) {
			FilterTreeNode childNode = (FilterTreeNode) e.nextElement();
			childNode.isShown = b;
			setAllChildVisible(childNode, b);
		}
	}

	public Object getChild(Object parent, int index) {
		int count = 0;
		for (int i = 0; i < treeModel.getChildCount(parent); i++) {
			final Object child = treeModel.getChild(parent, i);
			if (child instanceof FilterTreeNode) {
				if (((FilterTreeNode) child).isShown) {
					if (count == index) {
						return child;
					}
					count++;
				}
			} else if (count == index) {
				return child;
			} else {
				count++;
			}
		}
		return null;
	}

	public int getChildCount(Object parent) {
		int count = 0;
		for (int i = 0; i < treeModel.getChildCount(parent); i++) {
			final Object child = treeModel.getChild(parent, i);
			if (child instanceof FilterTreeNode) {
				if (((FilterTreeNode) child).isShown) {
					count++;
				}
			} else {
				count++;
			}
		}
		return count;
	}

	public boolean isLeaf(Object node) {
		if (node == null) {
			return true;
		} else {
			return treeModel.isLeaf(node);
		}
	}
}

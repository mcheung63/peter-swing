//import java.awt.EventQueue;
//
//import javax.swing.JButton;
//import javax.swing.JComboBox;
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.JRadioButton;
//import javax.swing.JScrollPane;
//import javax.swing.JTabbedPane;
//import javax.swing.JTable;
//import javax.swing.JTextField;
//import javax.swing.UIManager;
//import javax.swing.border.EmptyBorder;
//
//import com.peterswing.CommonLib;
//
//public class TestDisablePanel extends JFrame {
//	private JPanel contentPane;
//	private JTable table;
//	private JTextField textField;
//
//	public static void main(String[] args) {
//		try {
//			UIManager.setLookAndFeel("com.peterswing.white.PeterSwingWhiteLookAndFeel");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					TestDisablePanel frame = new TestDisablePanel();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public TestDisablePanel() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 835, 533);
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
//		//		contentPane.putClientProperty("disable", true);
//
//		JButton btnNewButton = new JButton("New button");
//		btnNewButton.setBounds(37, 32, 117, 25);
//		contentPane.add(btnNewButton);
//
//		JComboBox comboBox = new JComboBox();
//		comboBox.setBounds(37, 80, 134, 24);
//		contentPane.add(comboBox);
//
//		JScrollPane scrollPane = new JScrollPane();
//		scrollPane.setBounds(439, 36, 213, 215);
//		contentPane.add(scrollPane);
//
//		table = new JTable();
//		scrollPane.setViewportView(table);
//
//		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
//		tabbedPane.setBounds(37, 116, 335, 305);
//		contentPane.add(tabbedPane);
//
//		JPanel panel_1 = new JPanel();
//		tabbedPane.addTab("New tab", null, panel_1, null);
//
//		textField = new JTextField();
//		panel_1.add(textField);
//		textField.setColumns(10);
//
//		JRadioButton rdbtnNewRadioButton = new JRadioButton("New radio button");
//		panel_1.add(rdbtnNewRadioButton);
//
//		JButton btnNewButton_1 = new JButton("New button");
//		panel_1.add(btnNewButton_1);
//
//		JPanel panel = new JPanel();
//		tabbedPane.addTab("New tab", null, panel, null);
//
//		CommonLib.enableJComponent(contentPane, false);
//	}
//}

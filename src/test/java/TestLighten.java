//import java.awt.Color;
//import java.awt.EventQueue;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.Image;
//import java.awt.image.BufferedImage;
//import java.awt.image.RescaleOp;
//
//import javax.swing.ImageIcon;
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.JTextField;
//import javax.swing.UIManager;
//import javax.swing.border.EmptyBorder;
//
//import com.peterswing.white.TextFieldUI;
//import javax.swing.JSlider;
//import javax.swing.event.ChangeListener;
//import javax.swing.event.ChangeEvent;
//
//public class TestLighten extends JFrame {
//
//	private JPanel contentPane;
//	Image lostFocus_upperLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperLeft.png")).getImage();
//	Image lostFocus_middleLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLeft.png")).getImage();
//	Image lostFocus_lowerLeft = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerLeft.png")).getImage();
//	Image lostFocus_middleUpper = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleUpper.png")).getImage();
//	Image lostFocus_middleLower = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleLower.png")).getImage();
//	Image lostFocus_upperRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_upperRight.png")).getImage();
//	Image lostFocus_middleRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_middleRight.png")).getImage();
//	Image lostFocus_lowerRight = new ImageIcon(TextFieldUI.class.getResource("images/PTextField/lostFocus/PTextField_lostFocus_lowerRight.png")).getImage();
//	private JTextField textField;
//	static JSlider slider = new JSlider();
//	JTextField lblNewLabel;
//
//	public static void main(String[] args) {
//		try {
//			UIManager.setLookAndFeel("com.peterswing.white.PeterSwingWhiteLookAndFeel");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					TestLighten frame = new TestLighten();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	public TestLighten() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 900, 900);
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
//
//		final BufferedImage bb = b(lostFocus_upperLeft);
//		lblNewLabel = new JTextField("New label") {
//			public void paint(Graphics g) {
//				g.setColor(Color.white);
//				g.fillRect(0, 0, 1000, 1000);
//				g.drawImage(b(lostFocus_upperLeft), 0, 0, bb.getWidth(), bb.getHeight(), null);
//				g.drawImage(b(lostFocus_middleLeft), 0, 2, 2, getHeight() - 4, null);
//				g.drawImage(b(lostFocus_lowerLeft), 0, getHeight() - 2, 2, 2, null);
//
//				g.drawImage(b(lostFocus_middleUpper), 2, 0, getWidth() - 4, 2, null);
//				g.drawImage(b(lostFocus_middleLower), 2, getHeight() - 2, getWidth() - 4, 2, null);
//
//				g.drawImage(b(lostFocus_upperRight), getWidth() - 2, 0, 2, 2, null);
//				g.drawImage(b(lostFocus_middleRight), getWidth() - 2, 2, 2, getHeight() - 4, null);
//				g.drawImage(b(lostFocus_lowerRight), getWidth() - 2, getHeight() - 2, 2, 2, null);
//			}
//		};
//		lblNewLabel.setBounds(139, 24, 159, 28);
//		contentPane.add(lblNewLabel);
//
//		textField = new JTextField();
//		textField.setBounds(139, 65, 159, 28);
//		contentPane.add(textField);
//		textField.setColumns(10);
//
//		slider.addChangeListener(new ChangeListener() {
//			public void stateChanged(ChangeEvent e) {
//				lblNewLabel.repaint();
//			}
//		});
//		slider.setMinorTickSpacing(5);
//		slider.setMajorTickSpacing(25);
//		slider.setMaximum(200);
//		slider.setBounds(83, 150, 558, 29);
//		contentPane.add(slider);
//	}
//
//	public static BufferedImage b(Image img) {
//		float[] factors = new float[] { 1.4f, 1.4f, 1.4f };
//		float[] offsets = new float[] { 0.0f, 0.0f, 0.0f };
//		float f = (float) slider.getValue() / 100;
//		System.out.println(f);
//		RescaleOp rescaleOp = new RescaleOp(f, 30.0f, null);
//		return rescaleOp.filter(toBufferedImage(img), null);
//	}
//
//	public static BufferedImage toBufferedImage(Image img) {
//		if (img instanceof BufferedImage) {
//			return (BufferedImage) img;
//		}
//		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
//		Graphics2D bGr = bimage.createGraphics();
//		bGr.drawImage(img, 0, 0, null);
//		bGr.dispose();
//		return bimage;
//	}
//}


import hk.quantr.peterswing.CommonLib;
import java.awt.Color;
import javax.swing.UIManager;
import org.junit.Test;

/**
 *
 * @author Peter
 */
public class Test3 {

	@Test
	public void test() throws Exception {
		Color background = UIManager.getColor("Table.background");
		Color background2 = CommonLib.darker(background, 0.8f);
		System.out.println(background2.toString());
	}
}
